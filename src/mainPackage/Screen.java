package mainPackage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Console;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("unused")
public class Screen extends JPanel implements Runnable
{

	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 600, HEIGHT = 600;
	private Thread thread;

	public Thread getThread() {
		return this.thread;
	}
	public void sleepThread() throws InterruptedException {
		//Thread.sleep(1000);
		if(running == false)
		running = true;
		if(running == true)
		running = false;
	}
	public void runn() throws InterruptedException {
		thread.notify();

	}

	@SuppressWarnings("static-access")
	public void stopp() throws InterruptedException {
		thread.sleep(1000);
	}
	
	private int speedTick=100000;
	public void setSpeedTick(int value) {
		speedTick=value;
	}
	public int getSpeedTick() {
		return speedTick;
	}
	public void setPaused() {
		paused = true;
	}
	public void setNotPaused() {
		paused = false;
	}
	public boolean getPaused() {
		return paused;
	}
	public void addPoint() {
		points++;
	}
	public int getPoint() {
		return points;
	}
	public void setPoint(int value) {
		points=value;
	}
	
	private boolean paused = false;
	private boolean running = false;
	
	private BodyPart bodyPart;
	private ArrayList<BodyPart> snake;
	
	private Food apple;
	private ArrayList<Food> apples;
	private int xHead = 10, yHead = 10;
	private int size = 4;
	private int points = 0;
	
	private boolean right = true, left = false, up = false, down = false;
	private int ticks = 0;
	private Key key;
	private Random random;;
	
	int keyPressed=0;
	
	@Override
	protected void paintComponent(Graphics k) {
		super.paintComponent(k);
		k.setColor(Color.black);
		k.fillRect(0, 0, WIDTH, HEIGHT);

	}
	public Screen()
	{
		setFocusable(true);
		key = new Key();
		addKeyListener(key);
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		snake = new ArrayList<BodyPart>(); //Snake jako ArrayList
		
		random = new Random();
		apples = new ArrayList<Food>();

		start();
	
		
	}
	
	public boolean appleOnSnake(ArrayList<BodyPart> snake, int x, int y) {
		for(BodyPart object : snake) {
			if(x == (int) object.getxCoordinate() && y == (int) object.getyCoordinate() ) 
				return true;
		}
		return false;
			
	}

	public void setTesto() {
		
		setPaused();
		
		Frame.setTextPoint(0);
		setPoint(0);
		snake.clear();
		apples.clear();
		size=4;
		xHead=10;
		yHead=10;
		//snake = new ArrayList<BodyPart>(); //Snake jako ArrayList
		bodyPart = new BodyPart(xHead, yHead, 20);
		snake.add(bodyPart);
		right = true;
		left = false;
		up = false;
		down = false;
		//random = new Random();

		int xCoordinate;
		int yCoordinate;
		
		do {
			xCoordinate = random.nextInt(39);
			yCoordinate = random.nextInt(39);
		}while(appleOnSnake(snake, xCoordinate, yCoordinate));
		
		
		
		//apple = new Food(xCoordinate, yCoordinate , 20);
		//apples.add(apple);
		//running=true;
		//bodyPart = new BodyPart(xHead, yHead, 20);
		if(!running)
			start();
		setNotPaused();
		//start();

	}
	
	public void tick() 
	{
		if(!paused) {
			if(snake.size() == 0) //Tworzenie nowego weza
			{
				bodyPart = new BodyPart(xHead, yHead, 20);
				snake.add(bodyPart);
			}
			
			if(apples.size() == 0)
			{
				int xCoordinate;
				int yCoordinate;
				
				do {
					xCoordinate = random.nextInt(WIDTH/25);
					yCoordinate = random.nextInt(HEIGHT/25);
				}while(appleOnSnake(snake, xCoordinate, yCoordinate));
				
				
				
				apple = new Food(xCoordinate, yCoordinate , 20);
				apples.add(apple);
			}
			
			for(int i = 0; i<apples.size(); i++)
			{
				if(xHead == apples.get(i).getxCoordinate() && yHead == apples.get(i).getyCoordinate() )
				{
					size++;
					addPoint();
					Frame.setTextPoint(getPoint());
					apples.remove(i);
					i--;
				}
			}
			
			for(int i=0; i<snake.size(); i++)
			{
				if(xHead == snake.get(i).getxCoordinate() && yHead == snake.get(i).getyCoordinate())
				{
					if(i != snake.size() - 1) {
						JFrame end = new JFrame();
						JPanel mid = new JPanel();
						end.add(mid, BorderLayout.PAGE_END);
						JLabel speedStatus = new JLabel("Koniec Gry!" , JLabel.CENTER);
						end.setPreferredSize(new Dimension(200,100));
						end.pack();
						end.setResizable(false);
						end.setLocationRelativeTo(null);
						end.setVisible(true);
						end.add(speedStatus);
						stop();
					}
					
					
				}
			}
		
			if(xHead < 0) xHead =WIDTH/20; //40
			if(xHead > HEIGHT/20) xHead = 0;
			if(yHead < 0) yHead = HEIGHT/20;
			if(yHead > HEIGHT/20) yHead = 0;
	
				
				
			ticks++;
			//System.out.println("doszlo");
			if(ticks > speedTick )
			{
				if(right) xHead++;
				if(left) xHead--;
				if(up) yHead--;
				if(down) yHead++;
				
				ticks = 0;
				bodyPart =  new BodyPart(xHead, yHead, 20);
				snake.add(bodyPart);
				
				if(snake.size() > size)
				{
					snake.remove(0);
				}
			}
		}
	}
	
	public void paint(Graphics g)

	{	g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,WIDTH,HEIGHT);
		//g.clearRect(0,0,WIDTH,HEIGHT);
		
		
		for(int i = 0; i < WIDTH / 20; i++)
		{
			//g.drawLine(i * 20, 0 , i * 20, HEIGHT);	//punkt x1, punkt y1, punkt xkoncowy,punkt ykocnoiwy
		}
		for(int i = 0; i < HEIGHT; i++)
		{
			//g.drawLine(0, i * 20, WIDTH ,i * 20);
		}
		
		for(int i = 0; i < snake.size(); i++)	/////////////Rysowanie Snake
		{
			snake.get(i).draw(g);
		}
		for(int i = 0; i<apples.size(); i++)
		{
			apples.get(i).draw(g);
		}
		
	}
	
	public void start() 
	{
		running = true;
		thread = new Thread(this, "Game Loop");
		thread.start();
	}
	
	public void stop() {
		running = false;
		try {
		thread.join();
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		while(running) 
		{
			tick();
			repaint();
		}
	}
	
	private class Key implements KeyListener{
		
		@Override
		public void keyPressed(KeyEvent e)
		{

				int key = e.getKeyCode();
				if(key == KeyEvent.VK_RIGHT && !left)
				{
					up=false;
					down=false;
					right=true;
				}
				if(key == KeyEvent.VK_LEFT && !right)
				{
					up=false;
					down=false;
					left=true;
				}
				if(key == KeyEvent.VK_UP && !down)
				{
					left=false;
					right=false;
					up=true;
				}
				if(key == KeyEvent.VK_DOWN && !up)
				{
					left=false;
					right=false;
					down=true;
				}
				
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
		}

		
	}
}






