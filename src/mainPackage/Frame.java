package mainPackage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.SliderUI;

@SuppressWarnings("unused")
public class Frame extends JFrame implements ActionListener {

	static final int SPEED_MIN = 10000;
	static final int SPEED_MAX = 150000;
	static final int SPEED_INIT = 100000;
	private int valueOfSpeed;
	
	public void setValueOfSpeed(int value) {
		valueOfSpeed=value;
	}
	public int getValueOfSpeed() {
		return valueOfSpeed;
	}
	
	JButton buttonRestart = new JButton("Restart");
	JButton buttonPause = new JButton("Pause");
	JSlider sliderSpeed = new JSlider(JSlider.HORIZONTAL, SPEED_MIN, SPEED_MAX, SPEED_INIT);
	JLabel speedStatus = new JLabel("Speed: 30%" , JLabel.CENTER);
	public static JLabel pointStatus = new JLabel("Points: 0", JLabel.CENTER);
	Screen s;

	public static void setTextPoint(int value) {
		pointStatus.setText("Points: " + value);
	}
	private static final long serialVersionUID = 1L;

	public Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setPreferredSize(new Dimension(800, 800));
		setTitle("Snake");
		setResizable(false);
		init();

	}
	

	public void init() {
		
		s = new Screen();
		add(s);

		pack();
		setLocationRelativeTo(null);
		setVisible(true);

		buttonRestart.setFocusable(false);
		buttonPause.setFocusable(false);

		buttonRestart.addActionListener(this);
		buttonPause.addActionListener(this);
		
		sliderSpeed.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				System.out.println(getValueOfSpeed());
				s.setSpeedTick(((JSlider) ce.getSource()).getValue());
				setValueOfSpeed(((JSlider) ce.getSource()).getValue());
				if(getValueOfSpeed()<135000)
				speedStatus.setText("Speed: "+ ((((getValueOfSpeed()-SPEED_MAX)*(-1))/14000))*10 + "%");
				else
				speedStatus.setText("Speed: MIN");
			}
			
		});
		
		sliderSpeed.setPaintLabels(true);
		sliderSpeed.setFocusable(false);
		
		JPanel bottomPanel = new JPanel();

		add(bottomPanel, BorderLayout.PAGE_END);
		bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		bottomPanel.add(buttonRestart);
		bottomPanel.add(buttonPause);
		bottomPanel.add(sliderSpeed);
		bottomPanel.add(speedStatus);

		JPanel pointsPanel = new JPanel();

		add(pointsPanel, BorderLayout.PAGE_START);
		pointsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		pointsPanel.add(pointStatus);
	}

	public static void main(String[] args) {
		new Frame();
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		s.repaint();
		
		if (arg0.getSource() == buttonRestart) {
			s.setTesto();

		}

		if (arg0.getSource() == buttonPause) {
			if (!s.getPaused()) {
				s.setPaused();
				buttonPause.setText("Resume");
				//System.out.println(sliderSpeed.getValue());
			} else {
				s.setNotPaused();
				buttonPause.setText("Pause");
			}
		}
	}

}
